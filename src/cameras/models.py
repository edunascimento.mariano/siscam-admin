from django.db import models
from base_app.models import BaseModel
from address.models import Endereco
from users.models import Usuario


class Camera(BaseModel):
    id_camera = models.AutoField(primary_key=True)
    url_camera = models.URLField("Url da câmera")
    cep = models.ForeignKey(Endereco, verbose_name="CEP", blank=False, null=True, on_delete=models.SET_NULL)

    def __str__ (self):
        return self.url_camera

    class Meta:
        verbose_name = "Câmera"
        verbose_name_plural = "Câmeras"

class CameraUsuario(BaseModel):
    id_camera_usuario = models.AutoField(primary_key=True)
    id_camera = models.ForeignKey(Camera, verbose_name="Câmera", on_delete=models.CASCADE)
    id_usuario = models.ForeignKey(Usuario, verbose_name="Usuário", on_delete=models.CASCADE)
    data_uso = models.DateField("Data de uso", auto_now=True)

    def __str__(self) -> str:
        return str(self.id_camera)

    class Meta:
        verbose_name = "Câmera"
        verbose_name_plural = "Câmeras"
