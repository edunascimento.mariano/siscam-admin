from django.db import models


class BaseModel(models.Model):
    ativo = models.BooleanField(default=True)
    data_criacao = models.DateField(auto_now_add=True)
    data_modificacao = models.DateField(auto_now=True)

    class Meta:
        abstract=True

