from django.contrib import admin
from .models import Endereco, Cidade, Uf


@admin.register(Uf)
class UfAdmin(admin.ModelAdmin):
    pass


@admin.register(Cidade)
class CidadeAdmin(admin.ModelAdmin):
    pass

@admin.register(Endereco)
class EnderecoAdmin(admin.ModelAdmin):
    list_filter = ('cidade_id',)