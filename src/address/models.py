from django.db import models
from base_app.models import BaseModel


class Uf(BaseModel):
    uf_id = models.AutoField(primary_key=True)
    estado = models.CharField("Estado", max_length=100)

    def __str__(self):
        return self.estado

    class Meta:
        verbose_name="Estado"
        verbose_name_plural = "Estados"


class Cidade(BaseModel):
    cidade_id = models.AutoField(primary_key=True)
    cidade = models.CharField("Cidade", max_length=100)
    uf_id = models.ForeignKey(Uf, verbose_name="Estado", on_delete=models.CASCADE)

    def __str__(self):
        return self.cidade

    class Meta:
        verbose_name="Cidade"
        verbose_name_plural = "Cidades"

class Endereco(BaseModel):
    cep = models.CharField("CEP", primary_key=True, max_length=9, db_index=True)
    cidade_id = models.ForeignKey(Cidade, verbose_name="Cidade", on_delete=models.CASCADE)

    def __str__(self):
        return str(self.cep)

    class Meta:
        verbose_name="Endereço"
        verbose_name_plural = "Endereços"