# Generated by Django 4.1.4 on 2022-12-15 20:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_alter_usuario_hashed_password'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuario',
            name='acess_token',
            field=models.CharField(editable=False, max_length=300, null=True),
        ),
        migrations.AlterField(
            model_name='usuario',
            name='refresh_token',
            field=models.CharField(editable=False, max_length=300, null=True),
        ),
    ]
