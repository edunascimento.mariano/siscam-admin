from django.db import models
from base_app.models import BaseModel
from address.models import Endereco
import uuid


class Usuario(BaseModel):
    id_usuario = models.CharField(
        primary_key=True,
        default=uuid.uuid4, 
        editable=False,
        max_length=100
    )
    nome = models.CharField("Nome", max_length=100, unique=False)
    sobrenome = models.CharField("Sobrenome", max_length=100, unique=False)
    email = models.EmailField("email", unique=True)
    hashed_password = models.CharField("Password", max_length=300, unique=False)
    acess_token = models.CharField(max_length=300, unique=False, editable=False, null=True)
    refresh_token = models.CharField(max_length=300, unique=False, editable=False, null=True)
    cep = models.ForeignKey(Endereco, verbose_name="CEP", blank=False, null=True, on_delete=models.SET_NULL)

    def __str__(self) -> str:
        return self.email