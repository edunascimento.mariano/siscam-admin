from django.contrib import admin
from .models import Usuario
from cameras.models import CameraUsuario
from tabbed_admin import TabbedModelAdmin


class CameraUsuarioInline(admin.StackedInline):
    model = CameraUsuario
    extra = 0


@admin.register(Usuario)
class UsuarioAdmin(TabbedModelAdmin):
     inlines = [CameraUsuarioInline]
     list_display = ['nome', 'email', 'ativo']
     tab_overview = (
        (None, {
            'fields': ( 'ativo', 'nome', 'email','hashed_password')
        }),
        
    )
     tab_camera = (
        CameraUsuarioInline,
    )
    
     tabs = [
        ('Usuário', tab_overview),
        ('Camera', tab_camera)
    ]