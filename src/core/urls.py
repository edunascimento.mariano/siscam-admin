from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('center/', admin.site.urls),
]


admin.AdminSite.site_header = "Siscam"
admin.AdminSite.site_title = "Siscam"
admin.AdminSite.index_title = "Siscam admin"
